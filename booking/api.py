from __future__ import unicode_literals

def get_notification_config():
	return { "for_doctype":
		{
			"Booking Details": { "docstatus": (">", 0) },
		},
		"for_module_doctypes": {
        	"Booking Details": "Booking Details",
    	},
    	"for_module": {
	        "Booking": "booking.api.notification_count",
	    }
	}

def notification_count():
	return 20
