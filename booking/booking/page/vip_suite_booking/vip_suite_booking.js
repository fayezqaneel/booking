frappe.pages['vip-suite-booking'].on_page_load = function(wrapper) {
	frappe.ui.make_app_page({
		parent: wrapper,
		title: __('VIP Suite Booking'),
		single_column: true
	});

	wrapper.vip_suite_booking = new frappe.vipsuitebooking(wrapper);

	frappe.breadcrumbs.add("Booking");
}

frappe.vipsuitebooking = Class.extend({
	init: function(wrapper) {
		var me = this;
		// 0 setTimeout hack - this gives time for canvas to get width and height
		setTimeout(function() {
			me.setup(wrapper);
			me.get_data();
		}, 0);
	},

	setup: function(wrapper) {
		var me = this;
		this.wrapper = wrapper;
		this.elements = {
			layout: $(wrapper).find(".layout-main"),
			from_date: wrapper.page.add_date(__("From Date")),
			to_date: wrapper.page.add_date(__("To Date")),
			authority:wrapper.page.add_field({label: "Authority",fieldname:"authority", fieldtype: "Link",options:"Customer Group"}),
			refresh_btn: wrapper.page.set_primary_action(__("Refresh"),
				function() { me.get_data(); }, "fa fa-refresh"),
			pdf_btn: wrapper.page.set_secondary_action(__("Export PDF"),
				function() { me.make_pdf(); }, "fa fa-refresh")
		};

		this.elements.no_data = $('<div class="alert alert-warning">' + __("No Data") + '</div>')
			.toggle(false)
			.appendTo(this.elements.layout);

		this.elements.vipsuitebooking_wrapper = $('<div class="vipsuitebooking_wrapper text-center"></div>')
			.appendTo(this.elements.layout);

		this.options = {
			from_date: frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			to_date: frappe.datetime.get_today()
		};

		// set defaults and bind on change
		$.each(this.options, function(k, v) {
			me.elements[k].val(frappe.datetime.str_to_user(v));
			me.elements[k].on("change", function() {
				me.options[k] = frappe.datetime.user_to_str($(this).val());
				me.get_data();
			});
		});

		// bind refresh
		this.elements.refresh_btn.on("click", function() {
			me.get_data(this);
		});
	},

	get_data: function(btn) {
		var me = this;
		frappe.call({
			method: "booking.booking.doctype.booking_details.booking_details.get_vip_booking",
			args: {
				start: this.options.from_date,
				end: this.options.to_date,
				authority: this.wrapper.page.fields_dict.authority.input.value
			},
			btn: btn,
			callback: function(r) {
				if(!r.exc) {
					me.options.data = r.message;
					me.render()
				}
			}
		});
	},
	render:function(){
		var me = this;
		me.elements.vipsuitebooking_wrapper.html(me.options.data);
	},
	make_pdf:function(){

		var url = "/api/method/booking.booking.doctype.booking_details.booking_details.get_vip_booking_pdf?";
		var params = [];
		if(this.options.from_date){
			params.push("start="+this.options.from_date);
		}
		if(this.options.to_date){
			params.push("end="+this.options.to_date);
		}
		if(this.wrapper.page.fields_dict.authority.input.value){
			params.push("authority="+this.wrapper.page.fields_dict.authority.input.value);
		}else{
			params.push("authority=");
		}
		url += params.join("&");
		window.open(
			frappe.urllib.get_full_url(url));
	}



});
