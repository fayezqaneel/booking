frappe.pages['per-booking-report'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Per Booking Report',
		single_column: true
	});
	wrapper.per_booking_report = new frappe.per_booking_report(wrapper);

	frappe.breadcrumbs.add("Booking","Booking Details");
}

frappe.per_booking_report = Class.extend({
	init: function(wrapper) {
		var me = this;
		// 0 setTimeout hack - this gives time for canvas to get width and height
		setTimeout(function() {
			me.setup(wrapper);
			me.get_data();
		}, 0);
	},

	setup: function(wrapper) {
		var me = this;
		this.select = {
			search_option: "Customer",
		};
		this.check = {
			attach_fax: "on",
		};
		this.wrapper = wrapper;
		$(wrapper.page.menu_btn_group).find('.menu-btn-group-label').text("Export PDF");
		this.elements = {
			layout: $(wrapper).find(".layout-main"),
			from_date: wrapper.page.add_date(__("From Date")),
			to_date: wrapper.page.add_date(__("To Date")),
			search_option:wrapper.page.add_field({label: "Search on",fieldname:"search_option", fieldtype: "Select",default:"Customer",options:"Customer\nAuthority"}),

			refresh_btn: wrapper.page.set_primary_action(__("Refresh"),
				function() { me.get_data(); }, "fa fa-refresh"),
			pdf_btn: wrapper.page.add_menu_item(__("Export Summary PDF"),
				function() { me.make_pdf(); }),
				pdf_all_btn: wrapper.page.add_menu_item(
					 __("Export Detailed PDF"),
					 function(){
						 me.make_full_pdf();
					 }
				)
		};
		this.elements["customer"] = wrapper.page.add_field({label: "Customer",fieldname:"customer", fieldtype: "Link",options:"Customer"});
		this.elements["authority"] = wrapper.page.add_field({label: "Authority",fieldname:"authority", fieldtype: "Link",options:"Customer Group"})
		this.elements["attach_fax"] = wrapper.page.add_field({label: "Attach Fax", fieldtype: "Check",default:"Yes",options:"Yes\nNo"});
		if(this.select.search_option=="Customer"){
			$(me.elements["authority"].$input).parents(".frappe-control").hide();
		}else{
			$(me.elements["customer"].$input).parents(".frappe-control").hide();
		}

		this.elements.no_data = $('<div class="alert alert-warning">' + __("No Data") + '</div>')
			.toggle(false)
			.appendTo(this.elements.layout);

		this.elements.per_booking_report_wrapper = $('<div class="per_booking_report_wrapper text-center"></div>')
			.appendTo(this.elements.layout);



		this.options = {
			from_date: frappe.datetime.add_months(frappe.datetime.get_today(), -1),
			to_date: frappe.datetime.get_today()
		};

		this.links = {
			customer: '',
			authority: ''
		};

		// set defaults and bind on change
		$.each(this.options, function(k, v) {
			me.elements[k].val(frappe.datetime.str_to_user(v));

			me.elements[k].on("change", function() {
				me.options[k] = frappe.datetime.user_to_str($(this).val());
				me.get_data();
			});
		});
		console.log(me.elements);
		me.elements["search_option"].value = this.select.search_option;
		me.elements["attach_fax"].value = this.check.attach_fax;
		$(me.elements["attach_fax"].$input).on("change", function() {
			console.log($(this).val());
			me.check["attach_fax"] = me.check["attach_fax"]=="on"?"off":"on";
			me.get_data();
		});
		$(me.elements["search_option"].$input).on("change", function() {
			console.log($(this).val());
			me.select["search_option"] = $(this).val();
			if(me.select.search_option=="Customer"){
				$(me.elements["authority"].$input).val("");
				$(me.elements["authority"].$input).parents(".frappe-control").hide();
				$(me.elements["customer"].$input).parents(".frappe-control").show();
			}else{
				$(me.elements["customer"].$input).val("");
				$(me.elements["customer"].$input).parents(".frappe-control").hide();
				$(me.elements["authority"].$input).parents(".frappe-control").show();
			}
			me.get_data();
		});
		$.each(this.links, function(k, v) {
			if(me.elements[k]){
				me.elements[k].value = v;
				$(me.elements[k].$input).on("change", function() {
					me.links[k] = $(this).val();
					console.log(me.links);
					me.get_data();
				});
			}
		});

		// bind refresh
		this.elements.refresh_btn.on("click", function() {
			me.get_data(this);
		});



	},

	get_data: function(btn) {
		var me = this;
		frappe.call({
			method: "booking.booking.doctype.booking_details.booking_details.get_booking_per_customer",
			args: {
				start: this.options.from_date,
				end: this.options.to_date,
				customer: this.links.customer,
				authority: this.links.authority,
				attach_fax: this.check.attach_fax,
				search_option:this.select.search_option
			},
			btn: btn,
			callback: function(r) {
				if(!r.exc) {
					me.options.data = r.message;
					me.render();
					$(me.wrapper).find(".make_pdf_export").on("click", function(e) {
						e.preventDefault();
						//alert($(this).attr("data-booking"));
						me.make_single_pdf($(this).attr("data-booking"));
					});
				}
			}
		});
	},
	render:function(){
		var me = this;
		me.elements.per_booking_report_wrapper.html(me.options.data);
	},
	make_pdf:function(){
		var url = "/api/method/booking.booking.doctype.booking_details.booking_details.get_pdf_per_customer?";
		var params = [];
		if(this.options.from_date){
			params.push("start="+this.options.from_date);
		}
		if(this.options.to_date){
			params.push("end="+this.options.to_date);
		}
		if(this.links.customer){
			params.push("customer="+this.links.customer);
		}else{
			params.push("customer=");
		}
		if(this.links.authority){
			params.push("authority="+this.links.authority);
		}else{
			params.push("authority=");
		}
		if(this.check.attach_fax){
			params.push("attach_fax="+this.check.attach_fax);
		}else{
			params.push("attach_fax=on");
		}
		if(this.select.search_option){
			params.push("search_option="+this.select.search_option);
		}else{
			params.push("search_option=Customer");
		}
		url += params.join("&");
		window.open(
			frappe.urllib.get_full_url(url));
	},
	make_single_pdf:function(booking){
		var url = "/api/method/booking.booking.doctype.booking_details.booking_details.get_single_booking?id="+booking;
		window.open(
			frappe.urllib.get_full_url(url));
	},
	make_full_pdf:function(booking){
		var url = "/api/method/booking.booking.doctype.booking_details.booking_details.get_full_booking?";
		var params = [];
		if(this.options.from_date){
			params.push("start="+this.options.from_date);
		}
		if(this.options.to_date){
			params.push("end="+this.options.to_date);
		}
		if(this.links.customer){
			params.push("customer="+this.links.customer);
		}else{
			params.push("customer=");
		}
		if(this.links.authority){
			params.push("authority="+this.links.authority);
		}else{
			params.push("authority=");
		}
		if(this.check.attach_fax){
			params.push("attach_fax="+this.check.attach_fax);
		}else{
			params.push("attach_fax=on");
		}
		if(this.select.search_option){
			params.push("search_option="+this.select.search_option);
		}else{
			params.push("search_option=Customer");
		}
		url += params.join("&");
		window.open(
			frappe.urllib.get_full_url(url));
	}




});
