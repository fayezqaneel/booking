// Copyright (c) 2016, Fayez Qandeel and contributors
// For license information, please see license.txt


frappe.ui.form.on('Booking Details', {
  arrival_channel:function(frm){
    //console.log(locals);
      if(frm.doc.arrival_channel=="Normal"){
        frm.doc.arrival_channel_cost = "0.00";
        frm.refresh_field("arrival_channel_cost");
      }else{
        var count  = parseInt(frm.doc.additional_passengers_number) + parseInt(frm.fields_dict["passengers"].grid.grid_rows.length)
        function calculate(frm,prices,count){
          for(var i in prices){
            var price = prices[i];
            var from_value = price.label.split("-")[0];
            var to_value = price.label.split("-")[1];
            if(count>=from_value && count<=to_value){
              frm.doc.arrival_channel_cost = price.value;
            }
          }
          frm.refresh_field("arrival_channel_cost");
        }
        if(!locals["VIP Suite Prices"]){
          frappe.call({
              method:"booking.booking.doctype.booking_details.booking_details.get_prices_list",
              args: {},
              callback: function(r) {
                locals["VIP Suite Prices"] = r.message;
                calculate(frm,r.message,count);
              }
          });
        }else{
            calculate(frm,locals["VIP Suite Prices"],count);
        }
      }
  },
	onload:function(frm){
		$("body").on("grid-rendered",function(event, grid_obj){
			if(grid_obj.doctype=="Passengers"){
				grid_obj.wrapper.attr("id","passengers_table");
				grid_obj.grid_buttons.append('<button  class="btn btn-xs btn-default grid-print-landing"> Print Landing Card</button>');
				$("#passengers_table .grid-print-landing").bind("click",function(e){
					var passengers = grid_obj.get_selected().join(",");
					var port_of_last_departure = frm.doc.port_of_last_departure||"";
					var length_of_stay_in_the_uk = frm.doc.length_of_stay_in_the_uk||"";
					if(grid_obj.get_selected().length>0){
						window.open(
							frappe.urllib.get_full_url("/api/method/booking.booking.doctype.booking_details.booking_details.landing_cards?"
							+"passengers="+passengers+"&length="+length_of_stay_in_the_uk+"&port="+port_of_last_departure));
					}else{
						msgprint("Please select at least one passenger");
					}
				});

			}
		});
    if(frm.doc.amendments && frm.doc.amendments.length>0){
      frm.set_df_property("penalties", "hidden", 0);
      frm.set_df_property("penalties_heading", "hidden", 0);
      frm.set_df_property("penalties_total", "hidden", 0);
    }
    if(!frm.doc.penalties_total){
      frm.doc.penalties_total = "0.00";
      frm.refresh_field("penalties_total");
      frm.doc.arrival_channel_cost = "0.00";
      frm.refresh_field("arrival_channel_cost");
    }
    if(frm.doc.arrival_channel=="Normal"){
        frm.doc.arrival_channel_cost = "0.00";
        frm.refresh_field("arrival_channel_cost");
      }else{
        var count  = parseInt(frm.doc.passengers_count);
        function calculate(frm,prices,count){
          for(var i in prices){
            var price = prices[i];
            var from_value = price.label.split("-")[0];
            var to_value = price.label.split("-")[1];
            if(count>=from_value && count<=to_value){
              frm.doc.arrival_channel_cost = price.value;
            }
          }
          frm.refresh_field("arrival_channel_cost");
        }
        if(!locals["VIP Suite Prices"]){
          frappe.call({
              method:"booking.booking.doctype.booking_details.booking_details.get_prices_list",
              args: {},
              callback: function(r) {
                locals["VIP Suite Prices"] = r.message;
                calculate(frm,r.message,count);
              }
          });
        }else{
            calculate(frm,locals["VIP Suite Prices"],count);
        }
      }
	},
	 booking_reference: function(frm) {
        if(frm.doc.booking_reference==""){
        	frm.set_value("booking_status","Pending");
        }else{
        	frm.set_value("booking_status","Confirmed");
        }
    },
    validate: function(frm){
      if(!frm.doc.name){
        return frappe.call({
            method:"booking.booking.doctype.booking_details.booking_details.get_booking_docs",
            args: {
                date:frm.doc.date
            },
            callback: function(r) {
              var assigned_cars = 0;
              for(var i in r.message){

                for(var j in r.message[i].assigned_cars){
                  if(r.message[i].assigned_cars[j].supplier=="Own Cars" && r.message[i].assigned_cars[j].service_name =="Transfer"){
                    assigned_cars++;
                  }
                }
              }
              //alert(assigned_cars);
              if(assigned_cars>0){
                frappe.confirm(
                    'You have '+assigned_cars+' car(s) on transfer jobs on that day, do you want to automatically re-assign it?',
                    function(){
                        //window.close();
                        return false;
                    },
                    function(){
                        return false;
                    }
                );

              }else{
                return true;
              }

            }
          });
      }else{
        return true;
      }
      //return false;
      // if(frm.doc.arrival_channel){
      //   if(!frm.doc.attach_fax_scan||frm.doc.attach_fax_scan==""){
      //     msgprint(__("Please attach fax scan"));
      //     //return false;
      //   }
      //   if(frm.doc.arrival_channel=="VIP Suite" && (!frm.doc.attach_telex||frm.doc.attach_telex=="")){
      //     msgprint(__("Please attach telex scan"));
      //     //return false;
      //   }
      // }
    },
    attach_fax_scan: function(frm) {
        if(frm.doc.attach_fax_scan && frm.doc.attach_fax_scan!=""){
          console.log(frm.doc.attach_fax_scan);
          var ext = frm.doc.attach_fax_scan.split('.');
          var newname = frm.doc.name+"-fax."+ext[1];

          frappe.call({
            method:"booking.booking.doctype.booking_details.booking_details.rename_file",
            args: {
                file:frm.doc.attach_fax_scan.replace("/files/",""),
                newname:newname
            },
            callback: function(r) {
              frm.doc.attach_fax_scan = "/files/"+newname;
            }
          });
        }
    },
    attach_telex: function(frm) {
        if(frm.doc.attach_telex && frm.doc.attach_telex!=""){
          console.log(frm.doc.attach_telex);
          var ext = frm.doc.attach_telex.split('.');
          var newname = frm.doc.name+"-telex."+ext[1];

          frappe.call({
            method:"booking.booking.doctype.booking_details.booking_details.rename_file",
            args: {
                file:frm.doc.attach_telex.replace("/files/",""),
                newname:newname
            },
            callback: function(r) {
              frm.doc.attach_telex = "/files/"+newname;
            }
          });
        }
    },
    setup:function(frm){
        frappe.call({
            method:"booking.booking.doctype.booking_details.booking_details.get_vip_suites",
            args: {},
            callback: function(r) {
              frappe.vip_suites = r.message;
              console.log(frappe.vip_suites,"window.vip_suites");
            }
        });

    },
    booking_ref_no:function(frm){
      if(frm.doc.booking_ref_no && frm.doc.booking_ref_no!=""){
        frm.doc.bookingstatus = "Confirmed";
        frm.refresh_fields();
      }else{
        frm.doc.bookingstatus= "Pending";
        frm.refresh_fields();
      }
    },
    additional_passengers_number:function(frm){
      frm.doc.passengers_count = parseInt(frm.doc.additional_passengers_number) + parseInt(frm.fields_dict["passengers"].grid.grid_rows.length);
      frm.refresh_field("passengers_count");
    },
    refresh:function(frm){
      if(frm.doc.name && frm.doc.name!=""){
        //   for(var i in frm.fields_dict){
        //     frm.set_df_property(i, "read_only", 1);
        //   }

        //   frm.page.set_primary_action(__('Amend'), function() {
        //     for(var i in frm.fields_dict){
        //       frm.set_df_property(i, "read_only", 0);
        //     }
        //     frm.page.set_primary_action(__('Save'),function(){ frm.save();});
        //   });
         }
    }
    // number_of_passengers: function(frm) {
    // 	if(frm.doc.number_of_passengers>0){
    // 		frm.set_df_property("passengers", "read_only",1);
    // 		jQuery("[data-fieldtype=Table][data-fieldname=passengers]").css("position","relative").append("<div class='disable-grid'>");
    // 		jQuery("[data-fieldtype=Table][data-fieldname=passengers]").find(".disable-grid").css({
    // 			"position":"absolute",
    // 			"top":"0px",
    // 			"left":"0px",
    // 			"width":"100%",
    // 			"height":"100%",
    // 			"z-index":"99999",
    // 			"background":"#000",
    // 			"opacity":"0.1"

    // 		});
    // 	}else{
    // 		jQuery("[data-fieldtype=Table][data-fieldname=passengers]").find(".disable-grid").remove();
    // 		frm.set_df_property("passengers", "read_only",0);
    // 	}

    // },
    // refresh:function(frm) {
    // 	if(frm.doc.number_of_passengers>0){
    // 		frm.set_df_property("passengers", "read_only",1);
    // 		jQuery("[data-fieldtype=Table][data-fieldname=passengers]").css("position","relative").append("<div class='disable-grid'>");
    // 		jQuery("[data-fieldtype=Table][data-fieldname=passengers]").find(".disable-grid").css({
    // 			"position":"absolute",
    // 			"top":"0px",
    // 			"left":"0px",
    // 			"width":"100%",
    // 			"height":"100%",
    // 			"z-index":"99999",
    // 			"background":"#000",
    // 			"opacity":"0.1"

    // 		});
    // 	}else{
    // 		jQuery("[data-fieldtype=Table][data-fieldname=passengers]").find(".disable-grid").remove();
    // 		frm.set_df_property("passengers", "read_only",0);
    // 	}

    // }
});
cur_frm.set_query("driver", "assigned_cars", function(doc, cdt, cdn) {
var d = locals[cdt][cdn];
  return{
    filters: {
        designation: "Driver",
      }
  };
});
cur_frm.set_query("officer", "assigned_pr_officers", function(doc, cdt, cdn) {
var d = locals[cdt][cdn];
  return{
    filters: {
        designation: "PR Officer",
      }
  };
});

cur_frm.set_query("authorized_by", function(doc, cdt, cdn) {
var d = locals[cdt][cdn];
  return{
    filters: {
        designation: "Manager",
      }
  };
});
cur_frm.set_query("signed_by", function(doc, cdt, cdn) {
var d = locals[cdt][cdn];
  return{
    filters: {
        designation: "Manager",
      }
  };
});

var fetched_customers = {};
function fetch_values(frm,doc){
  var customer = fetched_customers[doc.customer];
  for(var i in frm.doc.passengers){
      if(doc.customer==frm.doc.passengers[i].customer){
        var target = frm.doc.passengers[i];
        target.id_number = customer.id_number;
        target.fullname = customer.prefix+" "+customer.title +customer.customer_name;
        target.gov_officials = customer.gov_officials;
        target.prefix = customer.prefix;
        target.title = customer.title;
        target.first_name = customer.first_name;
        target.middle_name = customer.middle_name;
        target.last_name = customer.last_name;
        target.dob = customer.date_of_birth;
        target.gender = customer.gender;
        target.nationality = customer.nationality;
        target.passport_number = customer.passport_number;
        target.passport_place_of_issue = customer.passport_place_of_issue;
        target.tel = customer.qatari_tel;
        target.uk_telephone = customer.uk_telephone;
        target.email = customer.customer_email;
        target.full_arabic_name = customer.full_arabic_name;
        target.attachments = customer.passenger_attachments;
      }
  }
  refresh_field("passengers");
}
frappe.ui.form.on("Passengers", "customer", function(frm, cdt, cdn) {

  frm.doc.passengers_count = parseInt(frm.doc.additional_passengers_number) + parseInt(frm.fields_dict["passengers"].grid.grid_rows.length);
  refresh_field("passengers_count");
  var doc = locals[cdt][cdn];

  if(!fetched_customers[doc.customer]){
      frappe.call({
        method:"booking.booking.doctype.booking_details.booking_details.get_customer",
        args: {
            name:doc.customer
        },
        callback: function(r) {
          fetched_customers[doc.customer] = r.message;
          fetch_values(frm,doc);
        }
      });
   }else{
      fetch_values(frm,doc);
   }
});
// frappe.ui.form.on("VIP Suite Details", "booking_ref_no", function(frm, cdt, cdn) {
//     var doc = locals[cdt][cdn];
//     var grid = frm.fields_dict["vip_suite_details"].grid;
//     fname = grid.grid_rows_by_docname[doc.name];
//     console.log(fname,"fnamefnamefname");
//     if(fname.doc.booking_ref_no && fname.doc.booking_ref_no!=""){
//       fname.doc.status = "Confirmed";
//       frm.refresh_fields();
//     }else{
//       fname.doc.status = "Pending";
//       frm.refresh_fields();
//     }
// });

function assign_doc(doc,frm){
	if(doc.type=="Verbal"){
		doc.from = doc.from_employee;
	}else if(doc.type=="Other"){
		doc.from = doc.from_other;
	}else{
		doc.from = doc.from_customer_group;
	}
	frm.refresh_fields();
}

frappe.ui.form.on("Documents and Verbal Instructions", "from_employee", function(frm, cdt, cdn) {
		var doc = locals[cdt][cdn];
		assign_doc(doc,frm);
});
frappe.ui.form.on("Documents and Verbal Instructions", "from_other", function(frm, cdt, cdn) {
		var doc = locals[cdt][cdn];
		assign_doc(doc,frm);
});
frappe.ui.form.on("Documents and Verbal Instructions", "from_customer_group", function(frm, cdt, cdn) {
		var doc = locals[cdt][cdn];
		assign_doc(doc,frm);
});

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
frappe.ui.form.on("Documents and Verbal Instructions", "attachment", function(frm, cdt, cdn) {
		var doc = locals[cdt][cdn];

		if(doc.attachment && doc.attachment!=""){
			console.log(frm.fields_dict["documents_and_verbal_instructions"].grid);
			var counter = frm.fields_dict["documents_and_verbal_instructions"].grid.grid_rows.length;
			counter = zeroPad(counter, 3);
			var ext = doc.attachment.split('.');
			var newname = frm.doc.name+"-"+doc.type+"-"+counter+"."+ext[ext.length-1];
			frappe.call({
				method:"booking.booking.doctype.booking_details.booking_details.rename_file",
				args: {
						file:doc.attachment.replace("/files/",""),
						newname:newname
				},
				callback: function(r) {
					doc.attachment = "/files/"+newname;
					frm.refresh_fields();
				}
			});
		}

});

frappe.ui.form.on("Amendments", {
  amendments_add: function(frm, cdt, cdn) {
    if(frm.fields_dict["amendments"].grid.grid_rows.length>=0){
      frm.set_df_property("penalties", "hidden", 0);
      frm.set_df_property("penalties_heading", "hidden", 0);
      frm.set_df_property("penalties_total", "hidden", 0);
    }
  },
  amendments_remove: function(frm, cdt, cdn) {
    console.log(frm.fields_dict["amendments"].grid.grid_rows.length);
    if(frm.fields_dict["amendments"].grid.grid_rows.length==1){
      frm.set_df_property("penalties", "hidden", 1);
      frm.set_df_property("penalties_heading", "hidden", 1);
      frm.set_df_property("penalties_total", "hidden", 1);
    }
  }
});


function update_penalties_total(frm, cdt, cdn){

  var total = 0.00;
  for(var i in locals[cdt]){
    var row = locals[cdt][i];
    total += row.value_and_sc;
  }
  cur_frm.doc.penalties_total = total;
  cur_frm.refresh_field("penalties_total");
}

frappe.ui.form.on("Booking Penalties", {
  penalties_remove: function(frm, cdt, cdn) {
    if(frm.fields_dict["penalties"].grid.grid_rows.length!=1){
      setTimeout(function(){
        update_penalties_total(frm, cdt, cdn);
      },0);
    }else{
      frm.set_value("penalties_total","0.00");
      // frm.doc.penalties_total = 0.00;
      // frm.refresh_field("penalties_total");
    }
  }
});

cur_frm.cscript.custom_penalty = function(frm, cdt, cdn) {
  var doc = locals[cdt][cdn];
  frappe.model.get_value("Penalties",{"type":doc.penalty}, "value",function(data){
      doc.value_and_sc = data.value + (parseFloat(data.value) * 0.20);
      cur_frm.refresh_fields();
      update_penalties_total(frm, cdt, cdn);
  }); 
};



frappe.ui.form.on("Assigned Cars", "supplier", function(frm, cdt, cdn) {
		var doc = locals[cdt][cdn];
		if(doc.supplier=="Own Cars"){
			doc.driver_label = doc.driver;
		}else{
			doc.driver_label = doc.external_driver;
		}
		frm.refresh_fields();
});
frappe.ui.form.on("Assigned Cars", "service_name", function(frm, cdt, cdn) {
  var doc = locals[cdt][cdn];
  if(frm.doc.flight_type=="Arrival"){
    var grid = frm.fields_dict["assigned_cars"].grid;
    fname = grid.grid_rows_by_docname[doc.name];
    for(var i in fname.docfields){
      if(fname.docfields[i].fieldname=="transfer_to"){
        fname.docfields[i].options = "Central London\nOther";
        fname.doc.transfer_to = "Central London";
        frm.refresh_fields();
      }
      if(fname.docfields[i].fieldname=="transfer_from"){
        var suites = [];
        for(var j in frappe.vip_suites){
          suites.push(frappe.vip_suites[j].name);
        }

        fname.docfields[i].options = suites.join("\n");
        fname.doc.transfer_from = suites[0];
        frm.refresh_fields();
      }
    }
  }
  if(frm.doc.flight_type=="Departure"){
    var grid = frm.fields_dict["assigned_cars"].grid;
    fname = grid.grid_rows_by_docname[doc.name];
    for(var i in fname.docfields){
      if(fname.docfields[i].fieldname=="transfer_from"){
        fname.docfields[i].options = "Central London\nOther";
        fname.doc.transfer_from = "Central London";
        frm.refresh_fields();
      }
      if(fname.docfields[i].fieldname=="transfer_to"){
        var suites = [];
        for(var j in frappe.vip_suites){
          suites.push(frappe.vip_suites[j].name);
        }

        fname.docfields[i].options = suites.join("\n");
        fname.doc.transfer_to = suites[0];
        frm.refresh_fields();
      }
    }
  }

  //console.log(fname,"fnamefname",grid.docfields);
  // if (fname && fname.length){
  //     var field = frappe.meta.get_docfield(fname[0].parent, "transfer_from", doc.name);
  //   }

  // console.log(field,"adasdas");
  // if(field) {
  //   field[property] = value;
  //   refresh_field(fieldname, table_field);
  // };


  //cur_frm.set_df_property("transfer_from", 'options', "test1\ntest2",doc.name,"assigned_cars")
  //set_field_options("transfer_from", ["test1","test2"]);
});

frappe.ui.form.on("Booking Details", "refresh", function(frm, cdt, cdn) {
  console.log($('div[data-fieldtype="Table"][data-fieldname="passengers"] .grid-buttons'));
});
