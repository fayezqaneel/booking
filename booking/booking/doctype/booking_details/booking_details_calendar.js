console.log(frappe.views.calendars);
function make_pdf(type,me){
	start = me.get_system_datetime(me.$cal.fullCalendar('getView').start);
	end = me.get_system_datetime(me.$cal.fullCalendar('getView').end);
	if(type=='guest'){
		window.open(
			frappe.urllib.get_full_url("/api/method/booking.booking.doctype.booking_details.booking_details.get_pdf?"
			+"start="+start+"&end="+end));
	}else{
		window.open(
			frappe.urllib.get_full_url("/api/method/booking.booking.doctype.booking_details.booking_details.get_pdf_by_provider?"
			+"start="+start+"&end="+end));
	}
}
frappe.views.calendar["Booking Details"] = {
	field_map: {
		"start": "booking_date",
		"end": "booking_date",
		"id": "name",
		"title": "calendar_name",
		"allDay": "allDay"
	},
	get_events_method: "booking.booking.doctype.booking_details.booking_details.get_events",
	filters: [

		{
			"fieldtype": "Link",
			"fieldname": "assigned_pr_officers",
			"options": "Employee",
			"label": __("PR Officer")
		},
		{
			"fieldtype": "Link",
			"fieldname": "assigned_cars",
			"options": "Employee",
			"label": __("Driver")
		},
		{
			"fieldtype": "Link",
			"fieldname": "authority",
			"options": "Customer Group",
			"label": __("authority")
		},
		{
			"fieldtype": "Link",
			"fieldname": "airway",
			"options": "Airway",
			"label": __("Airway")
		},
		{
			"fieldtype": "Link",
			"fieldname": "airport",
			"options": "Airport",
			"label": __("Airport")
		},
		{
			"fieldtype": "Select",
			"fieldname": "arrival_channel",
			"options": "Normal\nVIP",
			"label": __("Arrival Channel"),
			"default":"Normal"
		},
		{
			"fieldtype": "Link",
			"fieldname": "vip_suite",
			"options": "VIP Suite",
			"label": __("VIP Suite")
		}

	],
	get_css_class: function(data) {
		if(data.bookingstatus==="Confirmed") {
			return "success";
		} else if(data.bookingstatus==="Pending") {
			return "danger";
		} else {
			return "danger";
		}
	},
	make_page: function() {
		var me = this;
		this.parent = frappe.make_page();

		$(this.parent).on("show", function() {
			me.set_filters_from_route_options();
		});

		this.page = this.parent.page;
		var module = locals.DocType[this.doctype].module;
		this.page.set_title(__("Calendar") + " - " + __(this.doctype));

		frappe.breadcrumbs.add(module, this.doctype)

		this.add_filters();

		this.page.add_field({fieldtype:"Date", label:"Date",
			fieldname:"selected",
			"default": frappe.datetime.month_start(),
			input_css: {"z-index": 1},
			change: function() {
				var selected = $(this).val();
				if (selected) {
					me.$cal.fullCalendar("gotoDate", frappe.datetime.user_to_obj(selected));
				}
			}
		});

		this.page.set_primary_action(__("New"), function() {
			var doc = frappe.model.get_new_doc(me.doctype);
			frappe.set_route("Form", me.doctype, doc.name);
		});

		// add links to other calendars
		// $.each(frappe.boot.calendars, function(i, doctype) {
		// 	if(frappe.model.can_read(doctype)) {
		// 		me.page.add_menu_item(__(doctype), function() {
		// 			frappe.set_route("Calendar", doctype);
		// 		});
		// 	}
		// });

		this.page.add_action_item("Per booking report", function() {
			frappe.set_route("per-booking-report");
		});

		this.page.add_action_item("VIP suite booking summary ", function() {
			frappe.set_route("vip-suite-booking");
		});
		// this.page.add_action_item("Summary per authority", function() {
		// 	frappe.set_route("summary-per-authorit");
		// });
		this.page.add_action_item("Vehicle Detailed Graph", function() {
			frappe.set_route("vehicle-detailed-gra");
		});
		console.log(this.page.wrapper.find('.actions-btn-group .hidden-xs').html('Reports <span class="caret"></span>'));

		$(this.parent).on("show", function() {
			me.$cal.fullCalendar("refetchEvents");
		})
	}

}
