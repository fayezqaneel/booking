# -*- coding: utf-8 -*-
# Copyright (c) 2015, Fayez Qandeel and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
import json
from frappe.utils import cint, getdate, formatdate
from frappe import throw, _
from frappe.model.document import Document
from frappe.utils.print_format import report_to_pdf
from frappe.utils import get_files_path, get_site_path
from booking.messagebird.client import Client 
# Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
# MIT License. See license.txt



import pdfkit, os
from frappe.utils import scrub_urls
from frappe import _
from bs4 import BeautifulSoup
from pyPdf import PdfFileWriter, PdfFileReader

class BookingDetails(Document):
	def on_submit(self):
		self.submit_sms("Booking was sent to booking manager")
	def on_cancel(self):
		self.submit_sms("Booking was cancelled")
	def on_update(self):
		self.submit_sms("Booking was updated")
	def after_insert(self):
		self.submit_sms("Booking was inserted")
	def submit_sms(self,message):

		# frappe.sendmail(
		# 	recipients=recipients,
		# 	sender=frappe.session.user,
		# 	subject=subject,
		# 	message=message
		# )
		
		# ACCESS_KEY = 'wsiBReFddtC5O0PCKAGeHefhQ'
		# client = Client(ACCESS_KEY)
		# msg = client.message_create('QTR Embassy', '+447779950000', message)
		print message


def get_permission_query_conditions(user):
	if 'Driver' in frappe.get_roles() and user!="Administrator":
		emp = frappe.get_doc("Employee",{"user_id":frappe.session.user})
		docs = frappe.get_list('Assigned Cars',fields=["parent"],filters={"driver":emp.name})
		ids = []
		for item in docs:
			ids.append(item.parent)
		print "(`tabBooking Details`.name in ('%s'))" % ("','".join(ids))
		return "(`tabBooking Details`.name in ('%s'))" % ("','".join(ids))
	if 'PR Officer' in frappe.get_roles() and user!="Administrator":
		emp = frappe.get_doc("Employee",{"user_id":frappe.session.user})
		docs = frappe.get_list('PR Officers',fields=["parent"],filters={"officer":emp.name})
		ids = []
		for item in docs:
			ids.append(item.parent)
		print "(`tabBooking Details`.name in ('%s'))" % ("','".join(ids))
		return "(`tabBooking Details`.name in ('%s'))" % ("','".join(ids))
	return ""


def get_pdf(html, options=None, output = None):
	html = scrub_urls(html)
	html, options = prepare_options(html, options)
	fname = os.path.join("/tmp", "frappe-pdf-{0}.pdf".format(frappe.generate_hash()))

	try:
		pdfkit.from_string(html, fname, options=options or {})
		if output:
			append_pdf(PdfFileReader(file(fname,"rb")),output)
		else:
			with open(fname, "rb") as fileobj:
				filedata = fileobj.read()

	except IOError, e:
		if ("ContentNotFoundError" in e.message
			or "ContentOperationNotPermittedError" in e.message
			or "UnknownContentError" in e.message
			or "RemoteHostClosedError" in e.message):

			# allow pdfs with missing images if file got created
			if os.path.exists(fname):
				with open(fname, "rb") as fileobj:
					filedata = fileobj.read()

			else:
				frappe.throw(_("PDF generation failed because of broken image links"))
		else:
			raise

	finally:
		cleanup(fname, options)

	if output:
		return output

	return filedata

def append_pdf(input,output):
	# Merging multiple pdf files
    [output.addPage(input.getPage(page_num)) for page_num in range(input.numPages)]

def prepare_options(html, options):
	if not options:
		options = {}

	options.update({
		'print-media-type': None,
		'background': None,
		'images': None,
		'quiet': None,
		# 'no-outline': None,
		'encoding': "UTF-8",
		#'load-error-handling': 'ignore',

		# defaults
		'margin-right': '0mm',
		'margin-left': '0mm',
		'margin-bottom': '0mm',
		'margin-top': '0mm',
	})

	html, html_options = read_options_from_html(html)
	options.update(html_options or {})

	# cookies
	if frappe.session and frappe.session.sid:
		options['cookie'] = [('sid', '{0}'.format(frappe.session.sid))]

	# page size
	if not options.get("page-size"):
		options['page-size'] = frappe.db.get_single_value("Print Settings", "pdf_page_size") or "A4"

	return html, options

def read_options_from_html(html):
	options = {}
	soup = BeautifulSoup(html, "html5lib")

	# extract pdfkit options from html
	for html_id in ("margin-top", "margin-bottom", "margin-left", "margin-right", "page-size"):
		try:
			tag = soup.find(id=html_id)
			if tag and tag.contents:
				options[html_id] = tag.contents
		except:
			pass

	options.update(prepare_header_footer(soup))

	toggle_visible_pdf(soup)

	return soup.prettify(), options

def prepare_header_footer(soup):
	options = {}

	head = soup.find("head").contents
	styles = soup.find_all("style")

	bootstrap = frappe.read_file(os.path.join(frappe.local.sites_path, "assets/frappe/css/bootstrap.css"))
	fontawesome = frappe.read_file(os.path.join(frappe.local.sites_path, "assets/frappe/css/font-awesome.css"))

	# extract header and footer
	for html_id in ("header-html", "footer-html"):
		content = soup.find(id=html_id)
		if content:
			# there could be multiple instances of header-html/footer-html
			for tag in soup.find_all(id=html_id):
				tag.extract()

			toggle_visible_pdf(content)
			html = frappe.render_template("templates/print_formats/pdf_header_footer.html", {
				"head": head,
				"styles": styles,
				"content": content,
				"html_id": html_id,
				"bootstrap": bootstrap,
				"fontawesome": fontawesome
			})

			# create temp file
			fname = os.path.join("/tmp", "frappe-pdf-{0}.html".format(frappe.generate_hash()))
			with open(fname, "w") as f:
				f.write(html.encode("utf-8"))

			# {"header-html": "/tmp/frappe-pdf-random.html"}
			options[html_id] = fname

		else:
			if html_id == "header-html":
				options["margin-top"] = "0mm"

			elif html_id == "footer-html":
				options["margin-bottom"] = "0mm"

	return options

def cleanup(fname, options):
	if os.path.exists(fname):
		os.remove(fname)

	for key in ("header-html", "footer-html"):
		if options.get(key) and os.path.exists(options[key]):
			os.remove(options[key])

def toggle_visible_pdf(soup):
	for tag in soup.find_all(attrs={"class": "visible-pdf"}):
		# remove visible-pdf class to unhide
		tag.attrs['class'].remove('visible-pdf')

	for tag in soup.find_all(attrs={"class": "hidden-pdf"}):
		# remove tag from html
		tag.extract()






def get_pdf_file(html):
	frappe.local.response.filename = "report.pdf"
	frappe.local.response.filecontent = get_pdf(html, {"orientation": "Portrait","page-size":"A6","margin-right":"0.1mm","margin-left":"0.1mm","margin-top":"0.1mm","margin-bottom":"0.1mm"})
	frappe.local.response.type = "download"

@frappe.whitelist(allow_guest=True)
def get_prices_list():
	return frappe.get_list('VIP Suite Prices',fields=["label","value"])
@frappe.whitelist(allow_guest=True)
def get_customer(name):
	return frappe.get_doc('Customer',name)
@frappe.whitelist(allow_guest=True)
def rename_file(file,newname):
	import os
	os.rename(get_files_path(file),get_files_path(newname))

@frappe.whitelist(allow_guest=True)
def get_pdf_by_provider(start, end):
	values = {
		"start_date": getdate(start),
		"end_date": getdate(end)
	}

	data = frappe.db.sql("""select ac.*,bd.name as booking_details_name,bd.authority,bd.booking_date,bd.authorized_by,bd.signed_by,bd.submitted_by,bd.owner as ordered_by
		from  `tabAssigned Cars` ac
		left join `tabBooking Details` bd on ac.parent = bd.name
		where bd.booking_date is not null
		and bd.booking_date >= %(start_date)s
		and bd.booking_date <= %(end_date)s
		and ac.provider is not null
		group by ac.provider,bd.booking_date
		""",
		values, as_dict=True, update={"allDay": 1})
	if data:
		for item in data:
			item["provider_name"] =  frappe.get_value("Supplier",item["provider"],"supplier_name")
			item["assigned_cars"] = frappe.get_list("Assigned Cars",fields=["*"],filters = {"parent":item["booking_details_name"],"provider":item["provider"]})

			item["total"] = 0
			if item["assigned_cars"]:
				for car in item["assigned_cars"]:
					if "car" in car:
						car["car_object"] = frappe.get_doc("Vehicle",car["car"])
					if "price" in car:
						item["total"] += car["price"]
					car["passengers"] = frappe.get_list("Passengers",fields=["*"],filters = {"parent":car["parent"]})
			item["pr_officer"] = []
			item["pr_officer_string"] = ""
			item["assigned_pr_officers"] = frappe.get_list("PR Officers",fields=["*"],filters = {"parent":item["booking_details_name"]})
			if item["assigned_pr_officers"]:
				for officer in item["assigned_pr_officers"]:
						name = frappe.get_value("Employee",officer["officer"],"employee_name")
						item["pr_officer"].append(name)
				item["pr_officer_string"] = '-'.join(item["pr_officer"])
			if "authorized_by" in item:
				item["authorized_by"] = frappe.get_value("Employee",item["authorized_by"],"employee_name")
			if "assigned_by" in item:
				item["assigned_by"] = frappe.get_value("Employee",item["assigned_by"],"employee_name")
			if "ordered_by" in item:
				item["ordered_by"] = frappe.get_value("User",item["ordered_by"],"full_name")

		#print data
		context = {}
		context["clients"] = data
		context["frappe"] = frappe
		##print context
		html  = frappe.render_template("templates/includes/report_provider.html",
					context)
		report_to_pdf(html, orientation="Portrait")

@frappe.whitelist(allow_guest=True)
def landing_cards(passengers,length,port):
	#return "fd"
	passengers = passengers.split(',')
	passengers_string_parts = []
	for i in passengers:
		if not "New Passengers" in i:
			passengers_string_parts.append("'"+i+"'")
	passengers_string = ', '.join(passengers_string_parts)
	data  = frappe.db.sql("""select * from `tabPassengers`
		where name in (%s) """ % passengers_string,as_dict=True)
	#print data
	#return data
	context = {}
	context["passengers"] = data
	context["port"] = port
	context["length"] = length
	context["frappe"] = frappe
	##print context
	html  = frappe.render_template("templates/includes/landing_cards.html",
				context)
	get_pdf_file(html)

# @frappe.whitelist(allow_guest=True)
# def get_pdf(start, end):
# 	values = {
# 		"start_date": getdate(start),
# 		"end_date": getdate(end)
# 	}

# 	data = frappe.db.sql("""select p.*,bd.name as booking_details_name,bd.authority,bd.booking_date,bd.authorized_by,bd.signed_by,bd.submitted_by,bd.owner as ordered_by,bd.attach_fax_scan
# 		from  `tabPassengers` p
# 		left join `tabBooking Details` bd on p.parent = bd.name
# 		where bd.booking_date is not null
# 		and bd.booking_date >= %(start_date)s
# 		and bd.booking_date <= %(end_date)s
# 		group by bd.booking_date
# 		""",
# 		values, as_dict=True, update={"allDay": 1})
# 	if data:
# 		for item in data:
# 			item["assigned_cars"] = frappe.get_list("Assigned Cars",fields=["*"],filters = {"parent":item["booking_details_name"]})
# 			item["total"] = 0
# 			if item["assigned_cars"]:
# 				for car in item["assigned_cars"]:
# 					if "car" in car:
# 						car["car_object"] = frappe.get_doc("Vehicle",car["car"])
# 					if "price" in car:
# 						item["total"] += car["price"]
# 			item["pr_officer"] = []
# 			item["pr_officer_string"] = ""
# 			item["assigned_pr_officers"] = frappe.get_list("PR Officers",fields=["*"],filters = {"parent":item["booking_details_name"]})
# 			if item["assigned_pr_officers"]:
# 				for officer in item["assigned_pr_officers"]:
# 						name = frappe.get_value("Employee",officer["officer"],"employee_name")
# 						item["pr_officer"].append(name)
# 				item["pr_officer_string"] = '-'.join(item["pr_officer"])
# 			if "authorized_by" in item:
# 				item["authorized_by"] = frappe.get_value("Employee",item["authorized_by"],"employee_name")
# 			if "assigned_by" in item:
# 				item["assigned_by"] = frappe.get_value("Employee",item["assigned_by"],"employee_name")
# 			if "ordered_by" in item:
# 				item["ordered_by"] = frappe.get_value("User",item["ordered_by"],"full_name")

# 		#print data
# 		context = {}
# 		context["clients"] = data
# 		context["frappe"] = frappe
# 		#print context
# 		html  = frappe.render_template("templates/includes/report_guest.html",
# 					context)
# 		report_to_pdf(html, orientation="Portrait")

@frappe.whitelist()
def get_vip_suites():
	return frappe.get_list("VIP Suite")

@frappe.whitelist()
def get_booking_docs(date):
	items = frappe.get_list("Booking Details",fields=["*"],filters={"date":date})
	items_to_return = [];
	for item in items:
		items_to_return.append(frappe.get_doc("Booking Details",item["name"]))
	return items_to_return

@frappe.whitelist()
def get_events(start, end, filters=None):

	"""Returns events for Gantt / Calendar view rendering.

	:param start: Start date-time.
	:param end: End date-time.
	:param filters: Filters (JSON).
	"""
	import json
	filters = json.loads(filters)
	#print filters["assigned_pr_officers"]
	condition = ''
	values = {
		"start_date": getdate(start),
		"end_date": getdate(end)
	}
	officerjoin = ''
	driverjoin = ''
	officerselect = ''
	driverselect = ''

	if filters:
		if isinstance(filters, basestring):
			filters = json.loads(filters)

		if filters.get('assigned_pr_officers'):
			condition = 'and pro.officer =%(officer)s'
			values['officer'] = filters['assigned_pr_officers']
			officerjoin = " left join `tabPR Officers` pro on  bd.name = pro.parent "
			officerselect = ",pro.officer "
		if filters.get('assigned_cars'):
			condition = 'and ac.driver =%(driver)s'
			values['driver'] = filters['assigned_cars']
			driverjoin = " left join `tabAssigned Cars` ac on   bd.name = ac.parent "
			driverselect = ",ac.driver "
		if filters.get('authority'):
			condition = 'and bd.authority =%(authority)s'
			values['authority'] = filters['authority']
		if filters.get('airport'):
			condition = "and bd.airport='%s'" % filters.get('airport')
		if filters.get('airway'):
			condition = "and bd.airways='%s'" % filters.get('airway')
		if filters.get('arrival_channel'):
			condition = "and bd.arrival_channel='%s'" % filters.get('arrival_channel')


	# print """select bd.*,ac.name,ac.driver,pro.name,pro.officer
	# 	from  `tabAssigned Cars` ac
	# 	left join `tabPR Officers` pro on  ac.parent = pro.parent
	# 	left join `tabBooking Details` bd on  ac.parent = bd.name
	# 	where bd.booking_date is not null
	# 	and bd.booking_date >= %(start_date)s
	# 	and bd.booking_date <= %(end_date)s
	# 	{condition}""".format(condition=condition)

	data = frappe.db.sql("""select bd.*{officerselect}{driverselect}
		from  `tabBooking Details` bd
		{officerjoin}
		{driverjoin}
		where bd.booking_date is not null
		and bd.booking_date >= %(start_date)s
		and bd.booking_date <= %(end_date)s
		{condition}""".format(condition=condition,officerjoin=officerjoin,driverjoin=driverjoin,driverselect=driverselect,officerselect=officerselect),
		values, as_dict=True, update={"allDay": 1})
	if data:
		for item in data:
			item["passengers"] = frappe.get_list("Passengers",fields=["*"],filters = {"parent":item["name"]})
			if item["passengers"]:
				item["calendar_name"] = item["passengers"][0]["first_name"]+" "+item["passengers"][0]["last_name"]
				if item["arrival_channel"] == "Normal Channel":
					item["calendar_name"] = "N - "+item["calendar_name"]
				if item["arrival_channel"] == "VIP Suite":
					item["calendar_name"] = "V - "+item["calendar_name"]
				if item["arrival_channel"] == "Amiri":
					item["calendar_name"] = "A - "+item["calendar_name"]
			else:
				item["calendar_name"] = item["name"]
				if item["arrival_channel"] == "Normal Channel":
					item["calendar_name"] = "N - "+item["calendar_name"]
				if item["arrival_channel"] == "VIP Suite":
					item["calendar_name"] = "V - "+item["calendar_name"]
				if item["arrival_channel"] == "Amiri":
					item["calendar_name"] = "A - "+item["calendar_name"]
	# data = frappe.db.sql("""select bd.name as fax_local_reference,bd.assigned_pr_officers,bd.booking_date,bd.booking_type
	# 	from  `tabBooking Details` bd
	# 	left join `tabPR Officers` as pro on  bd.assigned_pr_officers = pro.officer
	# 	where bd.booking_date is not null
	# 	and bd.booking_date >= %(start_date)s
	# 	and bd.booking_date <= %(end_date)s
	# 	{condition}""".format(condition=condition),
	# 	values, as_dict=True, update={"allDay": 1})

	# data = frappe.db.sql("""select bd.name as fax_local_reference,bd.assigned_pr_officers,bd.booking_date,bd.booking_type
	# 	from  `tabBooking Details` bd
	# 	left join `tabPR Officers` as pro on  bd.assigned_pr_officers = pro.officer
	# 	where bd.booking_date is not null
	# 	and bd.booking_date >= %(start_date)s
	# 	and bd.booking_date <= %(end_date)s
	# 	{condition}""".format(condition=condition),
	# 	values, as_dict=True, update={"allDay": 1})

	return data
@frappe.whitelist()
def get_single_booking(id=None):
	if id:
		item = frappe.get_doc("Booking Details",id).as_dict();
		item["passenger"]= item["passengers"][0]
		document_type = False
		for document in item["documents_and_verbal_instructions"]:
			if document.type=="Fax" and  not document_type:
				document_type = document
		item.fax = document_type
		context = {}
		context["item"] = item
		context["frappe"] = frappe
		html  = frappe.render_template("templates/includes/report_single_booking_pdf.html",
					context)
		report_to_pdf(html, orientation="Portrait")

@frappe.whitelist()
def get_full_booking(start=None, end=None, customer=None,authority=None,attach_fax=None,search_option=None):
	params = {
		"start":start,
		"end":end,
		"customer":customer,
		"authority":authority,
		"attach_fax":attach_fax,
		"search_option":search_option
	}
	if params["search_option"] and params["search_option"]=="Authority":
		passengers = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")
	else:
		passengers = frappe.db.sql_list("""select distinct customer from `tabPassengers`""")
	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if (customer=="" and params["search_option"]!="Authority") or (authority=="" and params["search_option"]=="Authority") :
		for customer_item in passengers:
			if params["search_option"] and params["search_option"]=="Authority":
				customer_object = {"customer":customer_item}
				customer_item = "and bd.authority='%s'" % customer_item
			else:
				customer_object = {"customer":customer_item}
				customer_item = "and p.customer='%s'" % customer_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				inner join `tabPassengers` p on bd.name = p.parent
				where bd.booking_date is not null
				 %s
				 %s
				 %s

				""" % (start,end,customer_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			customer_object["items_list"] = all_data
			final_data.append(customer_object)
	else:
		if params["search_option"] and params["search_option"]=="Authority":
			customer_object = {"customer":params["authority"]}
			customer = "and bd.authority='%s'" % params["authority"]
		else:
			customer_object = {"customer":customer}
			customer = "and p.customer='%s'" % customer

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				inner join `tabPassengers` p on bd.name = p.parent
				where bd.booking_date is not null
				 %s
				 %s
				 %s

				""" % (start,end,customer), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		customer_object["items_list"] = all_data
		final_data.append(customer_object)
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe
	context["params"] = params
	html  = frappe.render_template("templates/includes/report_full_booking_pdf.html",
				context)
	report_to_pdf(html, orientation="Portrait")

@frappe.whitelist()
def get_pdf_per_customer(start=None, end=None, customer=None,authority=None,attach_fax=None,search_option=None):
	params = {
		"start":start,
		"end":end,
		"customer":customer,
		"authority":authority,
		"attach_fax":attach_fax,
		"search_option":search_option
	}
	if params["search_option"] and params["search_option"]=="Authority":
		passengers = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")
	else:
		passengers = frappe.db.sql_list("""select distinct customer from `tabPassengers`""")
	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if (customer=="" and params["search_option"]!="Authority") or (authority=="" and params["search_option"]=="Authority") :
		for customer_item in passengers:
			if params["search_option"] and params["search_option"]=="Authority":
				customer_object = {"customer":customer_item}
				customer_item = "and bd.authority='%s'" % customer_item
			else:
				customer_object = {"customer":customer_item}
				customer_item = "and p.customer='%s'" % customer_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				inner join `tabPassengers` p on bd.name = p.parent
				where bd.booking_date is not null
				 %s
				 %s
				 %s

				""" % (start,end,customer_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			customer_object["items_list"] = all_data
			final_data.append(customer_object)
	else:
		if params["search_option"] and params["search_option"]=="Authority":
			customer_object = {"customer":params["authority"]}
			customer = "and bd.authority='%s'" % params["authority"]
		else:
			customer_object = {"customer":customer}
			customer = "and p.customer='%s'" % customer

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				inner join `tabPassengers` p on bd.name = p.parent
				where bd.booking_date is not null
				 %s
				 %s
				 %s

				""" % (start,end,customer), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		customer_object["items_list"] = all_data
		final_data.append(customer_object)
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe
	context["params"] = params
	html  = frappe.render_template("templates/includes/report_pdf_per_customer.html",
				context)
	report_to_pdf(html, orientation="Portrait")
@frappe.whitelist()
def get_graph_data(cartype=None,start=None,end=None):

	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	if cartype=="Company Cars":
		data = {}
		data["labels"]  = frappe.db.sql_list("""select distinct license_plate from `tabVehicle`""")
		data["bar1"] = []
		data["bar2"] = []
		for car in data["labels"]:
			data["bar1"].append(frappe.db.sql("""
				select count(*) from `tabAssigned Cars` ac
				inner join `tabEmployee` e on ac.driver = e.name
				inner join `tabBooking Details` bd on ac.parent = bd.name
				where ac.driver is not null and e.assigned_vehicle = '%s'
				and ac.service_name = 'Transfer'
				%s
				%s
				""" %(car,start,end))[0][0])
			data["bar2"].append(frappe.db.sql("""
				select count(*) from `tabAssigned Cars` ac
				inner join `tabEmployee` e on ac.driver = e.name
				inner join `tabBooking Details` bd on ac.parent = bd.name
				where ac.driver is not null and e.assigned_vehicle = '%s'
				and ac.service_name = 'Disposal'
				%s
				%s
				""" %(car,start,end))[0][0])
		return data
	else:
		data = {}
		data["labels"]  = frappe.db.sql_list("""select distinct car_registration_number from `tabAssigned Cars`""")
		data["bar1"] = []
		data["bar2"] = []
		for car in data["labels"]:
			data["bar1"].append(frappe.db.sql("""
				select count(*) from `tabAssigned Cars` ac
				inner join `tabBooking Details` bd on ac.parent = bd.name
				where ac.driver is not null
				and ac.service_name = 'Transfer'
				and ac.car_registration_number = '%s'
				%s
				%s
				""" %(car,start,end))[0][0])
			data["bar2"].append(frappe.db.sql("""
				select count(*) from `tabAssigned Cars` ac
				inner join `tabBooking Details` bd on ac.parent = bd.name
				where ac.driver is not null
				and ac.service_name = 'Disposal'
				and ac.car_registration_number = '%s'
				%s
				%s
				""" %(car,start,end))[0][0])
		return data


@frappe.whitelist()
def get_vip_booking(start=None, end=None, authority=None):
	authorities = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")

	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if authority=="":
		for authority_item in authorities:
			authority_object = {"authority":authority_item}
			authority_item = "and bd.authority='%s'" % authority_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null  and bd.arrival_channel='VIP'
				 %s
				 %s
				 %s
				""" % (start,end,authority_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			authority_object["items_list"] = all_data
			final_data.append(authority_object)
	else:
		authority_object = {"authority":authority}
		authority = "and bd.authority='%s'" % authority

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null  and bd.arrival_channel='VIP'
				 %s
				 %s
				 %s
				""" % (start,end,authority), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		authority_object["items_list"] = all_data
		final_data.append(authority_object)
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe

	html  = frappe.render_template("templates/includes/report_vipsuitebooking.html",
				context)
	return html
@frappe.whitelist()
def get_booking_per_customer(start=None, end=None, customer=None,authority=None,attach_fax=None,search_option=None):
	params = {
		"start":start,
		"end":end,
		"customer":customer,
		"authority":authority,
		"attach_fax":attach_fax,
		"search_option":search_option
	}
	if params["search_option"] and params["search_option"]=="Authority":
		passengers = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")
	else:
		passengers = frappe.db.sql_list("""select distinct customer from `tabPassengers`""")
	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if (customer=="" and params["search_option"]!="Authority") or (authority=="" and params["search_option"]=="Authority") :
		for customer_item in passengers:
			if params["search_option"] and params["search_option"]=="Authority":
				customer_object = {"customer":customer_item}
				customer_item = "and bd.authority='%s'" % customer_item
			else:
				customer_object = {"customer":customer_item}
				customer_item = "and p.customer='%s'" % customer_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				inner join `tabPassengers` p on bd.name = p.parent
				where bd.booking_date is not null
				 %s
				 %s
				 %s

				""" % (start,end,customer_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			customer_object["items_list"] = all_data
			final_data.append(customer_object)
	else:
		if params["search_option"] and params["search_option"]=="Authority":
			customer_object = {"customer":params["authority"]}
			customer = "and bd.authority='%s'" % params["authority"]
		else:
			customer_object = {"customer":customer}
			customer = "and p.customer='%s'" % customer

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				inner join `tabPassengers` p on bd.name = p.parent
				where bd.booking_date is not null
				 %s
				 %s
				 %s

				""" % (start,end,customer), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		customer_object["items_list"] = all_data
		final_data.append(customer_object)
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe
	context["params"] = params
	html  = frappe.render_template("templates/includes/report_per_booking.html",
				context)
	return html

@frappe.whitelist()
def get_booking(start=None, end=None, authority=None):
	authorities = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")

	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if authority=="":
		for authority_item in authorities:
			authority_object = {"authority":authority_item}
			authority_item = "and bd.authority='%s'" % authority_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null
				 %s
				 %s
				 %s
				""" % (start,end,authority_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			authority_object["items_list"] = all_data
			final_data.append(authority_object)
	else:
		authority_object = {"authority":authority}
		authority = "and bd.authority='%s'" % authority

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null
				 %s
				 %s
				 %s
				""" % (start,end,authority), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		authority_object["items_list"] = all_data
		final_data.append(authority_object)
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe
	html  = frappe.render_template("templates/includes/report_summary_per_authority.html",
				context)
	return html
@frappe.whitelist()
def get_pdf_per_authority(start=None, end=None, authority=None):
	authorities = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")

	params = {
		"start":start,
		"end":end,
		"authority":authority
	}

	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if authority=="":
		for authority_item in authorities:
			authority_object = {"authority":authority_item}
			authority_item = "and bd.authority='%s'" % authority_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null
				 %s
				 %s
				 %s
				""" % (start,end,authority_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			authority_object["items_list"] = all_data
			final_data.append(authority_object)
	else:
		authority_object = {"authority":authority}
		authority = "and bd.authority='%s'" % authority

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null
				 %s
				 %s
				 %s
				""" % (start,end,authority), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		authority_object["items_list"] = all_data
		final_data.append(authority_object)
	# return final_data
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe
	context["params"] = params
	html  = frappe.render_template("templates/includes/report_summary_per_authority_pdf.html",
				context)
	report_to_pdf(html, orientation="Portrait")

@frappe.whitelist()
def get_vip_booking_pdf(start=None, end=None, authority=None):
	params = {
		"start":start,
		"end":end,
		"authority":authority
	}
	authorities = frappe.db.sql_list("""select distinct authority from `tabBooking Details`""")

	if start:
		start = "and bd.booking_date >='%s'" % getdate(start)
	else:
		start = ""
	if end:
		end = "and bd.booking_date <='%s'" % getdate(end)
	else:
		end = ""

	final_data = []
	if authority=="":
		for authority_item in authorities:
			authority_object = {"authority":authority_item}
			authority_item = "and bd.authority='%s'" % authority_item
			data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null  and bd.arrival_channel='VIP'
				 %s
				 %s
				 %s
				""" % (start,end,authority_item), as_dict=True, update={"allDay": 1})
			all_data = []
			for item in data:
				newitem = frappe.get_doc("Booking Details",item.name).as_dict()
				newitem.passenger = newitem.passengers[0]
				document_type = False
				for document in newitem.documents_and_verbal_instructions:
					if document.type=="Fax" and  not document_type:
						document_type = document
				newitem.fax = document_type
				all_data.append(newitem)
			authority_object["items_list"] = all_data
			final_data.append(authority_object)
	else:
		authority_object = {"authority":authority}
		authority = "and bd.authority='%s'" % authority

		data = frappe.db.sql("""select bd.*
				from  `tabBooking Details` bd
				where bd.booking_date is not null  and bd.arrival_channel='VIP'
				 %s
				 %s
				 %s
				""" % (start,end,authority), as_dict=True, update={"allDay": 1})
		all_data = []
		for item in data:
			newitem = frappe.get_doc("Booking Details",item.name).as_dict()
			newitem.passenger = newitem.passengers[0]
			document_type = False
			for document in newitem.documents_and_verbal_instructions:
				if document.type=="Fax" and  not document_type:
					document_type = document
			newitem.fax = document_type
			all_data.append(newitem)

		authority_object["items_list"] = all_data
		final_data.append(authority_object)
	# return final_data
	context = {}
	context["data"] = final_data
	context["frappe"] = frappe
	context["params"] = params
	html  = frappe.render_template("templates/includes/report_vipsuitebooking_pdf.html",
				context)
	report_to_pdf(html, orientation="Portrait")
