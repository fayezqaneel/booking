frappe.ui.form.on('Customer', {
	first_name: function(frm) {
        frm.doc.customer_name = frm.doc.first_name + " "+ frm.doc.middle_name + " "+frm.doc.last_name;
    },
    middle_name: function(frm) {
        frm.doc.customer_name = frm.doc.first_name + " "+ frm.doc.middle_name + " "+frm.doc.last_name;
    },
    last_name: function(frm) {
        frm.doc.customer_name = frm.doc.first_name + " "+ frm.doc.middle_name + " "+frm.doc.last_name;
    }
});