# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Booking",
			"color": "#700000",
			"icon": "octicon octicon-credit-card",
			"type": "module",
			"label": _("Booking")
		}
	]