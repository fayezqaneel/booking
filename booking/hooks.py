# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "booking"
app_title = "Booking"
app_publisher = "Fayez Qandeel"
app_description = "Booking app description comes later"
app_icon = "octicon octicon-credit-card"
app_color = "#990000"
app_email = "customvivvo@gmail.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/booking/css/booking.css"
# app_include_js = "/assets/booking/js/booking.js"

app_include_js = ["/assets/booking/js/customer.js","/assets/booking/js/extend.js"]


# include js, css files in header of web template
# web_include_css = "/assets/booking/css/booking.css"
# web_include_js = "/assets/booking/js/booking.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

doctype_list_js = {"Booking Details" : ["public/js/booking_details_list.js"]}


permission_query_conditions = {
	"Booking Details": "booking.booking.doctype.booking_details.booking_details.get_permission_query_conditions"
}

notification_config = "booking.api.get_notification_config"


# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "booking.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "booking.install.before_install"
# after_install = "booking.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "booking.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"booking.tasks.all"
# 	],
# 	"daily": [
# 		"booking.tasks.daily"
# 	],
# 	"hourly": [
# 		"booking.tasks.hourly"
# 	],
# 	"weekly": [
# 		"booking.tasks.weekly"
# 	]
# 	"monthly": [
# 		"booking.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "booking.install.before_tests"

# Overriding Whitelisted Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "booking.event.get_events"
# }
